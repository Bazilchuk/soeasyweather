//
//  WeatherVC.swift
//  WeatherKit
//
//  Created by Andrew on 19.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit

class WeatherVC: UIViewController {
    
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()        

        self.humidityLabel.text = "Humidity = \(ForecastService.shared.humidity)"
        self.pressureLabel.text = "Pressure = \(ForecastService.shared.pressure)"
        self.windSpeedLabel.text = "Wind speed = \(ForecastService.shared.windSpeed)"
        self.summaryLabel.text = "\(ForecastService.shared.summary)"
        
        print("temperature")
        print(ForecastService.shared.temperature)
        print("humidity")
        print(ForecastService.shared.humidity)
        print("precipProbability")
        print(ForecastService.shared.precipProbability)
        print("pressure")
        print(ForecastService.shared.pressure)
        print("summary")
        print(ForecastService.shared.summary)
        print("windSpeed")
        print(ForecastService.shared.windSpeed)
        // Do any additional setup after loading the view.
    }
}
