//
//  ViewController.swift
//  WeatherKit
//
//  Created by Andrew on 18.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import UIKit

class CurrentWeatherVC: UIViewController {
    
    @IBOutlet weak var cityTextLabel: UILabel!
    @IBOutlet weak var temperatureTextLabel: UILabel!
    @IBOutlet weak var summaryTextLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GeoByIPService.shared.getCurrentLatitudeLongitude { (currentIP) in
            if let currentIP = currentIP{
                DispatchQueue.main.async {
                    if let latitude = currentIP.latitude {
                        GeoByIPService.shared.coordinate.lat = latitude
                    }
                    if let longitude = currentIP.longitude {
                        GeoByIPService.shared.coordinate.long = longitude
                    }
                    
                    ForecastService.shared.getCurrentWeather(latitude: GeoByIPService.shared.coordinate.lat,
                                                             longitude: GeoByIPService.shared.coordinate.long,
                                                             completion: { (currentWeather) in
                                                                self.cityTextLabel.text = ForecastService.shared.timezone
                                                                if let currentWeather = currentWeather{
                                                                    if let temperature = currentWeather.temperature {
                                                                        let temperatureInF = Int(32 + Double(temperature) * (1.8))
                                                                        self.temperatureTextLabel.text = "\(temperature)°С & \(temperatureInF)°F"
                                                                    } else {
                                                                        self.temperatureTextLabel.text = "-"
                                                                    }
                                                                    if let summary = currentWeather.summary {
                                                                        self.summaryTextLabel.text = "\(summary)"
                                                                    } else {
                                                                        self.summaryTextLabel.text = "-"
                                                                    }
                                                                }
                    })
                }
            }
        }
    }
}








