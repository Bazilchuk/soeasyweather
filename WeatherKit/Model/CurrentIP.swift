//
//  CurrentIP.swift
//  WeatherKit
//
//  Created by Andrew on 25.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import Foundation

class CurrentIP {
    
    var latitude: String?
    var longitude: String?

    struct GeoKeys {
        static let ip = "ip"
        static let latitude = "latitude"
        static let longitude = "longitude"
    }
    
    init(geoDictionary: [String : Any]) {
        if let latitudeDouble = geoDictionary[GeoKeys.latitude] as? String {
            latitude = String(latitudeDouble)
        } else {
            latitude = nil
        }
        
        if let longitudeDouble = geoDictionary[GeoKeys.longitude] as? String {
            longitude = String(longitudeDouble)
        } else {
            longitude = nil
        }
    }
}
