//
//  File.swift
//  WeatherKit
//
//  Created by Andrew on 21.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import Foundation
import Alamofire


class GeoByIPService{
    
    static let shared = GeoByIPService()
    
    var coordinate: (lat: String, long: String) = ("", "")
    let inquiryBaseURL = URL(string: "https://api.2ip.ua/geo.json?ip=")
    
    func getCurrentLatitudeLongitude(completion: @escaping (CurrentIP?) -> Void) {
        
        if let inquiryURL = URL(string: "\(inquiryBaseURL!)"){
            Alamofire.request(inquiryURL).responseJSON(completionHandler: { (response) in
                if let geoDictionary = response.result.value as? [String : Any] {
                    let currentGeo = CurrentIP(geoDictionary: geoDictionary)
                    completion(currentGeo)
                } else {
                    completion(nil)
                }
            })
        }
    }
}
