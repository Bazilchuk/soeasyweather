//
//  ForecastService.swift
//  WeatherKit
//
//  Created by Andrew on 18.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import Foundation
import Alamofire

class ForecastService {
    
    static let shared = ForecastService()
    var timezone = ""
    var precipProbability = 0
    var temperature = 0
    var humidity = 0
    var pressure = 0.0
    var summary = ""
    var windSpeed = 0.0
    
    static let forecastAPIKey = "48bdf8f2798eb39e55f07f028424a14f"
    let forecastBaseURL = URL(string: "https://api.darksky.net/forecast/\(forecastAPIKey)")
    
    func getCurrentWeather(latitude: String, longitude: String, completion: @escaping (CurrentWeather?) -> Void) {
        if let forecastURL = URL(string: "\(forecastBaseURL!)/\(latitude),\(longitude)") {
            Alamofire.request(forecastURL).responseJSON(completionHandler: { (response) in
                if let jsonDictionary = response.result.value as? [String : Any] {
                    if let currentTimezone = jsonDictionary["timezone"] {
                        self.timezone = currentTimezone as! String
                    }
                    print(jsonDictionary)
                    if let dailyWeatherDictionary = jsonDictionary["daily"] as? [String : Any] {
                        
                        if let summaryData = dailyWeatherDictionary["summary"] as? String {
                            self.summary = String(summaryData)
                        }
                    }
                    if let currentWeatherDictionary = jsonDictionary["currently"] as? [String : Any] {
                        print(currentWeatherDictionary)
                        let currentWeather = CurrentWeather(weatherDictionary: currentWeatherDictionary)
                        if let precipDouble = currentWeatherDictionary["precipProbability"] as? Double {
                            self.precipProbability = Int(precipDouble * 100)
                        }
                        if let temperatureFahrenheit = currentWeatherDictionary["temperature"] as? Double {
                            self.temperature = Int((temperatureFahrenheit - 32) * 5 / 9)
                        }
                        if let humidityDouble = currentWeatherDictionary["humidity"] as? Double {
                            self.humidity = Int(humidityDouble * 100)
                        }
                        if let pressureDouble = currentWeatherDictionary["pressure"] as? Double {
                            self.pressure = Double(pressureDouble)
                        }
                        if let windSpeedDouble = currentWeatherDictionary["windSpeed"] as? Double {
                            self.windSpeed = Double(windSpeedDouble)
                        }
                        completion(currentWeather)
                    } else {
                        completion(nil)
                    }
                }
            })
        }
    }
}
