//
//  CurrentWeather.swift
//  WeatherKit
//
//  Created by Andrew on 18.03.2018.
//  Copyright © 2018 Andrew. All rights reserved.
//

import Foundation

class CurrentWeather {
    
    let temperature: Int?
    let humidity: Int?
    let precipProbability: Int?
    let summary: String?
    let icon: String?
    
    struct WeatherKeys {
        static let temperature = "temperature"
        static let humidity = "humidity"
        static let precipProbability = "precipProbability"
        static let summary = "summary"
        static let icon = "icon"
    }
    
    init(weatherDictionary: [String : Any]) {
        
        if let temperatureFahrenheit = weatherDictionary[WeatherKeys.temperature] as? Double {
//            T(°C) = (T(°F) - 32) × 5/9
            temperature = Int((temperatureFahrenheit - 32) * 5 / 9)
        } else {
            temperature = nil
        }
        
        if let humidityDouble = weatherDictionary[WeatherKeys.humidity] as? Double {
            humidity = Int(humidityDouble * 100)
        } else {
            humidity = nil
        }
        
        if let precipDouble = weatherDictionary[WeatherKeys.precipProbability] as? Double{
            precipProbability = Int(precipDouble * 100)
        } else {
            precipProbability = nil
        }
        
        summary = weatherDictionary[WeatherKeys.summary] as? String
        icon = weatherDictionary[WeatherKeys.icon] as? String
    }
}
